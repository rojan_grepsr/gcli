#!/bin/bash

folder1=$(find $VORTEX_PLUGINS_PATH/vortex-plugins-services -mindepth 1 -type d -printf "%f ")
folder2=$(find $VORTEX_PLUGINS_PATH/vortex-plugins-services-1 -mindepth 1 -type d -printf "%f ")

_gcliComplete()
{
    local cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "$folder1 $folder2" -- $cur) )
}

complete -F _gcliComplete gcli
