#!/bin/bash

#CHANGE THIS PATH TO YOUR VORTEX_PLUGINS_PATH
VORTEX_PLUGINS_PATH=/home/test/Documents/vortex-plugins


RED='\033[1;31m'
YELLOW='\033[1;93m'
NC='\033[0m' # No Color

if [ $VORTEX_PLUGINS_PATH == '/home/test/Documents/vortex-plugins' ]

then
	echo -e "\n${RED}Your VORTEX_PLUGINS_PATH is set to $VORTEX_PLUGINS_PATH"
	echo -e "${NC}Please change it to your vortex-plugins path in ${YELLOW}install.sh ${NC}file.\n"
	echo -e "${NC}Eg: VORTEX_PLUGINS_PATH=/home/rojan/Documents/vortex-plugins\n"
	exit 0
fi


if [[ -z $HOME ]]; then
	echo "Home directory not set."
else
	echo -e "\r\rStart installation process...."
	echo "Home directory is set to $HOME"
	mkdir -p $HOME/.config/gcli
	echo ".gcli directory created."
	echo "copy gcli to .gcli folder."
	cp gcli $HOME/.config/gcli
	echo "make gcli executable."
	chmod +x $HOME/.config/gcli/gcli
	echo "copy autocomplete.sh to .gcli."
	cp autocomplete.sh $HOME/.config/gcli
	echo "make autocomplete.sh executable."
	chmod +x $HOME/.config/gcli/autocomplete.sh
	echo "#gcli settings start" >> $HOME/.bashrc
	echo "set VORTEX_PLUGINS_PATH"
	echo export VORTEX_PLUGINS_PATH=$VORTEX_PLUGINS_PATH >> $HOME/.bashrc
	echo "sourcing gcli autocomplete"
	echo source "$HOME/.config/gcli/autocomplete.sh" >> $HOME/.bashrc
	echo "create gcli alias."
	echo alias gcli="$HOME/.config/gcli/gcli" >> $HOME/.bashrc
	echo "#gcli settings end" >> $HOME/.bashrc
	echo -e "\rInstallation complete :)"
	source $HOME/.bashrc
fi
