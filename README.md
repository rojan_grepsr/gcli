# gcli (Grepsr CLI) 

gcli (Grepsr Cli) is utility tool to run service using command line.

## Version
**0.2**

## Features

- Tab completion of report name
- No more cd into vortex-plugins-sdk

## Installation process

**In your terminal**

1. `git clone https://bitbucket.org/rojan_grepsr/gcli.git`

2. `cd gcli`

3. In line number 3 of gcli/install.sh file, set your vortex-plugins full path

	- `VORTEX_PLUGINS_PATH=/home/test/Documents/vortex-plugins` **CHANGE THIS**

4. `chmod +x install.sh`

5. `./install.sh`

**For Mac users**

Source .bashrc in your .bash_profile

`source .bashrc`


## Usages

Lists all reports starting from amazon_
	
	gcli run amazon_<tab><tab>

Shows help text.

	gcli help

Shows current version of gcli

	gcli version


## Known issues

- Does not support parameter passing through cli
- [~~Does not work on mac.~~](https://bitbucket.org/rojan_grepsr/gcli/issues/1/does-not-work-on-mac)

## Report issue/bug, request features

[Issue tracker](https://bitbucket.org/rojan_grepsr/gcli/issues)

## Change log
---
**v 0.2**

1. Finding directory names using find command.

2. Supports on mac os.
